/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.io.reader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import com.acme.domain.steps.Step;
import java.io.File;
import java.nio.file.Paths;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StepsFileReaderTest {

  private StepsFileReader reader;
  private File inputTextFile;

  @BeforeEach
  public void initialize() {
    this.inputTextFile = Paths.get("src/test/resources/input.txt").toFile();
  }

  @Test
  public void shouldSetAndGetInputTextFile() {
    StepsFileReader reader = new StepsFileReader().inputTextFile(inputTextFile);
    assertThat("Testing inputTextFile", inputTextFile, is(equalTo(reader.getInputTextFile())));
  }

  @Test
  public void shouldSkipBlankLines() {
    Integer size = 3;

    File inputText = Paths.get("src/test/resources/input_with_blank_lines.txt").toFile();
    StepsFileReader reader = new StepsFileReader().inputTextFile(inputText).readSteps();

    assertThat(size, is(equalTo(reader.getSteps().size())));
  }

  @Test
  public void shouldReturnStepsList() {
    Integer size = 3;

    File inputText = Paths.get("src/test/resources/input001.txt").toFile();
    StepsFileReader reader = new StepsFileReader().inputTextFile(inputText).readSteps();

    assertThat(size, is(equalTo(reader.getSteps().size())));
  }

  @Test
  public void shouldGetStepsInfo() {
    String title = "First step into the line 60min";
    Integer duration = 60;

    StepsFileReader reader = new StepsFileReader();
    Step step = reader.getStepsInfo("First step into the line 60min");

    assertThat(title, is(equalTo(step.getTitle())));
    assertThat(duration, is(equalTo(step.getDuration())));
  }

  @Test
  public void shouldExtractTimeFromTitle() {
    Integer expected = 45;

    StepsFileReader reader = new StepsFileReader();
    Integer stepDuration = reader.extractTimeFromTitle("First step into the line 45min");

    assertThat("Testing time extraction from title", expected, is(equalTo(stepDuration)));
  }

  @Test
  public void shouldReturnCorrectMaintenanceTime() {
    Integer expected = 5;

    StepsFileReader reader = new StepsFileReader();
    Integer maintenanceTime = reader.extractTimeFromTitle("Its time for maintenance");

    assertThat(expected, is(equalTo(maintenanceTime)));
  }
}
