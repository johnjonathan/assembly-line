/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.io.BufferedWriter;
import org.junit.jupiter.api.Test;

class MainTest {

  @Test
  void shouldInitialize() {
    Main main = spy(new Main());

    main.initialize(new String[] {"-f", "src/test/resources/input.txt"});

    verify(main, times(1)).initialize(new String[] {"-f", "src/test/resources/input.txt"});
  }

  @Test
  void argShouldBeValid() {
    Main main = new Main();
    boolean isValid = main.isArgValid(new String[] {"-f", "input.txt"});
    assertTrue(isValid);
  }

  @Test
  void argShouldBeNotValid() {
    Main main = new Main();
    boolean isInvalid = main.isArgValid(new String[] {"-n", "input.txt"});
    assertTrue(!isInvalid);
  }

  @Test
  void shouldReturnADashLine() {
    Main main = new Main();

    String dashed = main.getDashLine();

    assertThat(dashed, is(notNullValue()));
    assertThat(dashed.length(), is(equalTo(60)));
  }

  @Test
  void shouldCreateBufferedWriter() {
    Main main = new Main();

    BufferedWriter buffer = main.createBufferedWriter();

    assertThat(buffer, is(notNullValue()));
  }

  @Test
  void shouldPrintUsage() {
    Main main = spy(new Main());

    main.printUsage();

    verify(main, atLeastOnce()).printUsage();
  }
}
