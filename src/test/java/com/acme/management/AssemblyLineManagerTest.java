/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.management;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.io.FileMatchers.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.nio.file.Paths;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AssemblyLineManagerTest {

  private File inputTextFile;
  private String path;

  @BeforeEach
  void initialize() {
    this.path = "src/test/resources/input.txt";
    this.inputTextFile = Paths.get(path).toFile();
  }

  @Test
  void assemblyLineManagerWithTextFileTest() {
    AssemblyLineManager manager = new AssemblyLineManager();
    manager.withInputTextFile(path);

    assertThat(manager.getInputTextFile(), aFileWithCanonicalPath(containsString(path)));
    assertThat(manager.getInputTextFile(), aFileWithAbsolutePath(containsString(path)));
  }

  @Test
  void assemblyLineManagerWithFileTest() {
    AssemblyLineManager manager = new AssemblyLineManager();
    manager.withInputTextFile(inputTextFile);

    assertThat(manager.getInputTextFile(), anExistingFile());
  }

  @Test
  void shouldStartAndStopManager() {
    AssemblyLineManager manager = spy(new AssemblyLineManager());
    manager.withInputTextFile(path);
    manager.start();
    verify(manager, times(1)).start();
    verify(manager, times(1)).stop();
  }
}
