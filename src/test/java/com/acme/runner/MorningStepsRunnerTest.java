/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.runner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.acme.domain.steps.Step;
import com.acme.io.reader.StepsFileReader;
import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MorningStepsRunnerTest {

  private File inputTextFile;
  private StepsFileReader fileReader;
  private List<Step> stepsDisarray;

  @BeforeEach
  public void initialize() {
    this.inputTextFile = Paths.get("src/test/resources/input.txt").toFile();
    this.fileReader = new StepsFileReader().inputTextFile(inputTextFile).readSteps();
    this.stepsDisarray = fileReader.getSteps();
  }

  @Test
  void shouldPassStepsDisarray() {
    MorningStepsRunner runner = new MorningStepsRunner().stepsDisarray(stepsDisarray);
    assertThat(runner.getStepsDisarray(), is(notNullValue()));
  }

  @Test
  void shouldBeCalled() throws Exception {

    ExecutorService executor = Executors.newFixedThreadPool(10);

    List<Step> steps = executor.submit(new MorningStepsRunner().stepsDisarray(stepsDisarray)).get();

    assertThat(steps, not(emptyCollectionOf(Step.class)));
  }
}
