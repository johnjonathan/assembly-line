/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AbstractStepsCollectionTest {

  @Test
  void shouldInitialize() {
    Integer size = 2;
    List<Step> stepsDisarray =
        Stream.of(
                new Step.Builder().duration(60).title("First step 60min").build(),
                new Step.Builder().duration(30).title("Second step 30min").build())
            .collect(Collectors.toList());

    AbstractStepsCollection collection =
        Mockito.mock(AbstractStepsCollection.class, Mockito.CALLS_REAL_METHODS);
    collection.initialize(stepsDisarray);

    assertThat(collection.getSteps(), is(notNullValue()));
    assertThat(size, is(equalTo(collection.getStepsDisarray().size())));
  }

  @Test
  void add() {
    List<Step> stepsDisarray = Mockito.mock(ArrayList.class);
    Step step = new Step.Builder().duration(60).title("First step 60min").build();

    AbstractStepsCollection collection =
        Mockito.mock(AbstractStepsCollection.class, Mockito.CALLS_REAL_METHODS);
    collection.initialize(stepsDisarray);
    collection.add(step);

    assertThat(collection.getSteps(), hasItem(step));
  }
}
