/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.acme.visitor.StepCollectionVisitor;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AfternoonStepsCollectionTest {

  @Test
  void shouldAcceptVisitor() {
    List<Step> stepsDisarray = Mockito.mock(ArrayList.class);
    Step step = Mockito.mock(Step.class);
    StepCollectionVisitor visitor = Mockito.mock(StepCollectionVisitor.class);

    AfternoonStepsCollection collection = new AfternoonStepsCollection(stepsDisarray);
    collection.accept(visitor);

    verify(visitor, times(1)).visit(collection);
  }
}
