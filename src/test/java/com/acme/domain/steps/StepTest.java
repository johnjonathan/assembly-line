/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class StepTest {

  @Test
  void shouldBuildAFullStepObject() {
    Integer duration = 35;
    LocalDateTime beginsAt = LocalDateTime.now();
    String title = "Assembly line step title 35min";
    StepType type = StepType.ASSEMBLY_LINE;

    Step step =
        new Step.Builder().duration(duration).beginsAt(beginsAt).title(title).type(type).build();

    assertThat(duration, is(equalTo(step.getDuration())));
    assertThat(beginsAt, is(equalTo(step.getBeginsAt())));
    assertThat(title, is(equalTo(step.getTitle())));
    assertThat(type, is(equalTo(step.getType())));
  }
}
