/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LunchTimeStepTest {

  private Step step;

  @BeforeEach
  public void setUp() throws Exception {
    step = new LunchTimeStep().getStep();
  }

  @Test
  public void shouldReturnALunchTimeStep() {
    assertEquals(PeriodStartTime.LUNCH_PERIOD.beginsAt(), step.getBeginsAt());
    assertEquals(PeriodTimeDuration.LUNCH.time(), step.getDuration());
    assertEquals("Lunch time", step.getTitle());
    assertEquals("12:00 Lunch time", step.toString());
  }
}
