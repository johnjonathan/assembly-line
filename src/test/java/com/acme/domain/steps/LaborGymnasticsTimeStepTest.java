/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class LaborGymnasticsTimeStepTest {

  @Test
  void shouldBuildAndReturnStep() {
    Step lastStepBeforeGym =
        new Step.Builder()
            .duration(10)
            .title("Last step before Gymn")
            .beginsAt(LocalDateTime.now().withHour(16).withMinute(25))
            .type(StepType.ASSEMBLY_LINE)
            .build();

    Step step = new LaborGymnasticsTimeStep(lastStepBeforeGym).getStep();

    assertThat(0, is(equalTo(step.getDuration())));
    assertThat("Labor Gymnastics", is(equalTo(step.getTitle())));
    assertThat(
        lastStepBeforeGym.getBeginsAt().plusMinutes(lastStepBeforeGym.getDuration()),
        is(equalTo(step.getBeginsAt())));
    assertThat(StepType.LABOR_GYMNASTICS, is(equalTo(step.getType())));
    assertThat("16:35 Labor Gymnastics", is(equalTo(step.toString())));
  }
}
