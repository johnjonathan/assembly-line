/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.acme.visitor.StepCollectionVisitor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class MorningStepsCollectionTest {

  @Test
  void shouldInitialize() {
    Integer size = 2;
    List<Step> stepsDisarray =
        Stream.of(
                new Step.Builder().duration(60).title("First step 60min").build(),
                new Step.Builder().duration(30).title("Second step 30min").build())
            .collect(Collectors.toList());

    MorningStepsCollection collection = new MorningStepsCollection(stepsDisarray);

    assertThat(collection.getSteps(), is(notNullValue()));
    assertThat(collection.getStepsDisarray(), hasSize(size));
  }

  @Test
  void shouldAddStep() {
    List<Step> stepsDisarray = Mockito.mock(ArrayList.class);
    Step step = new Step.Builder().duration(60).title("First step 60min").build();

    MorningStepsCollection collection = new MorningStepsCollection(stepsDisarray);

    collection.add(step);

    assertThat(collection.getSteps(), hasItem(step));
  }

  @Test
  void shouldAcceptVisitor() {
    List<Step> stepsDisarray = Mockito.mock(ArrayList.class);
    Step step = Mockito.mock(Step.class);
    StepCollectionVisitor visitor = Mockito.mock(StepCollectionVisitor.class);

    MorningStepsCollection collection = new MorningStepsCollection(stepsDisarray);
    collection.accept(visitor);

    verify(visitor, times(1)).visit(collection);
  }
}
