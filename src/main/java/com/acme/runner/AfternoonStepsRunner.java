/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.runner;

import com.acme.domain.steps.AfternoonStepsCollection;
import com.acme.domain.steps.Step;
import com.acme.domain.steps.StepType;
import com.acme.visitor.AssemblyLineStepCollectionVisitor;
import java.util.List;
import java.util.concurrent.Callable;

public class AfternoonStepsRunner implements Callable<List> {

  private List<Step> stepsDisarray;

  public AfternoonStepsRunner stepsDisarray(List<Step> stepsDisarray) {
    this.stepsDisarray = stepsDisarray;
    return this;
  }

  @Override
  public List<Step> call() {
    AssemblyLineStepCollectionVisitor visitor = new AssemblyLineStepCollectionVisitor();
    AfternoonStepsCollection collection = new AfternoonStepsCollection(this.stepsDisarray);
    collection.accept(visitor);
    boolean hasLaborGymTime =
        collection.getSteps().stream()
            .map(Step::getType)
            .anyMatch(StepType.LABOR_GYMNASTICS::equals);
    if (!hasLaborGymTime) collection.getSteps().clear();
    return collection.getSteps();
  }
}
