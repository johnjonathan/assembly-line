/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.runner;

import com.acme.domain.steps.MorningStepsCollection;
import com.acme.domain.steps.Step;
import com.acme.domain.steps.StepType;
import com.acme.visitor.AssemblyLineStepCollectionVisitor;
import java.util.List;
import java.util.concurrent.Callable;

public class MorningStepsRunner implements Callable<List> {

  private List<Step> stepsDisarray;

  public MorningStepsRunner stepsDisarray(List<Step> stepsDisarray) {
    this.stepsDisarray = stepsDisarray;
    return this;
  }

  @Override
  public List<Step> call() {
    AssemblyLineStepCollectionVisitor visitor = new AssemblyLineStepCollectionVisitor();
    MorningStepsCollection collection = new MorningStepsCollection(this.stepsDisarray);
    collection.accept(visitor);
    boolean hasLunchTime =
        collection.getSteps().stream().map(Step::getType).anyMatch(StepType.LUNCH_TIME::equals);

    if (!hasLunchTime) collection.getSteps().clear();
    return collection.getSteps();
  }

  public List<Step> getStepsDisarray() {
    return this.stepsDisarray;
  }
}
