/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.management;

import com.acme.domain.steps.Step;
import com.acme.io.AssemblyLinePrintWriter;
import com.acme.io.reader.StepsFileReader;
import com.acme.runner.AfternoonStepsRunner;
import com.acme.runner.MorningStepsRunner;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AssemblyLineManager implements Manageable {

  private static final Logger logger = Logger.getAnonymousLogger();
  private StepsFileReader stepsFileReader;
  private ExecutorService executor;

  public AssemblyLineManager() {
    initialize();
  }

  @Override
  public void initialize() {
    this.stepsFileReader = new StepsFileReader();
    this.executor = Executors.newFixedThreadPool(10);
  }

  public AssemblyLineManager withInputTextFile(File inputTextFile) {
    this.stepsFileReader.inputTextFile(inputTextFile);
    return this;
  }

  public AssemblyLineManager withInputTextFile(String inputTextFile) {
    this.withInputTextFile(Paths.get(inputTextFile).toFile());
    return this;
  }

  @Override
  public void start() {
    stepsFileReader.readSteps();
    List<Step> stepsCollections = new ArrayList<>();

    try {

      while (stepsFileReader.getSteps().iterator().hasNext()) {
        List morning =
            executor
                .submit(new MorningStepsRunner().stepsDisarray(stepsFileReader.getSteps()))
                .get();
        List afternoon =
            executor
                .submit(new AfternoonStepsRunner().stepsDisarray(stepsFileReader.getSteps()))
                .get();
        executor
            .submit(
                () -> {
                  List<Step> steps =
                      (List<Step>)
                          Stream.concat(morning.stream(), afternoon.stream())
                              .collect(Collectors.toList());
                  stepsCollections.addAll(steps);
                })
            .get();
      }
      executor.submit(() -> AssemblyLinePrintWriter.println(stepsCollections)).get();
    } catch (ExecutionException e) {
      logger.log(Level.WARNING, e.getMessage(), e);
    } catch (InterruptedException e) {
      logger.log(Level.WARNING, e.getMessage(), e);
      Thread.currentThread().interrupt();
    } finally {
      this.stop();
    }
  }

  @Override
  public void stop() {
    this.executor.shutdown();
  }

  public File getInputTextFile() {
    return this.stepsFileReader.getInputTextFile();
  }
}
