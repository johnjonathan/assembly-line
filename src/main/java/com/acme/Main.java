/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme;

import com.acme.management.AssemblyLineManager;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Collections;

public class Main {

  public static void main(String[] args) {
    new Main().initialize(args);
  }

  public void initialize(String[] args) {
    if (isArgValid(args)) {
      new AssemblyLineManager().withInputTextFile(args[1]).start();
    } else {
      printUsage();
    }
  }

  public boolean isArgValid(String[] args) {
    return args.length > 0 && args[0].equals("-f") && !args[1].trim().isEmpty();
  }

  public String getDashLine() {
    return String.join("", Collections.nCopies(60, "="));
  }

  public BufferedWriter createBufferedWriter() {
    return new BufferedWriter(
        new OutputStreamWriter(new FileOutputStream(java.io.FileDescriptor.out)));
  }

  public void printUsage() {
    String dashedLine = getDashLine();

    try (PrintWriter writer = new PrintWriter(createBufferedWriter(), true); ) {
      writer.println(dashedLine);
      writer.println(" Usage: assembly-line -f [FILE_PATH]");
      writer.println(dashedLine);
    }
  }
}
