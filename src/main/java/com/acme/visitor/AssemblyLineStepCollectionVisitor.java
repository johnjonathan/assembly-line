/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.visitor;

import static com.acme.domain.steps.PeriodStartTime.AFTERNOON_PERIOD;
import static com.acme.domain.steps.PeriodStartTime.MORNING_PERIOD;
import static com.acme.domain.steps.StepType.ASSEMBLY_LINE;

import com.acme.domain.steps.*;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class AssemblyLineStepCollectionVisitor implements StepCollectionVisitor {

  private Integer timeConsumed;
  private PeriodTimeDuration periodTimeDuration;

  public AssemblyLineStepCollectionVisitor() {
    this.timeConsumed = 0;
  }

  private boolean stillHaveTimeAvailable(Integer time) {
    if (periodTimeDuration.equals(PeriodTimeDuration.AFTERNOON)) {
      return IntStream.of(timeConsumed, time).sum() < periodTimeDuration.time();
    }
    return IntStream.of(timeConsumed, time).sum() <= periodTimeDuration.time();
  }

  private void updateTimeConsumed(Integer time) {
    timeConsumed += time;
  }

  private void updateStep(Step step, LocalDateTime beginsAt) {
    step.type(ASSEMBLY_LINE).beginsAt(beginsAt.plusMinutes(timeConsumed));
  }

  public void setPeriodTimeDuration(PeriodTimeDuration timeDuration) {
    this.periodTimeDuration = timeDuration;
  }

  @Override
  public void visit(MorningStepsCollection morningStepsCollection) {
    timeConsumed = 0;
    setPeriodTimeDuration(PeriodTimeDuration.MORNING);
    for (Iterator<Step> iterator = morningStepsCollection.getStepsDisarray().iterator();
        iterator.hasNext(); ) {
      Step step = iterator.next();
      if (stillHaveTimeAvailable(step.getDuration())) {
        updateStep(step, MORNING_PERIOD.beginsAt());
        iterator.remove();
        updateTimeConsumed(step.getDuration());
        morningStepsCollection.add(step);
      } else if (timeConsumed.equals(periodTimeDuration.time())) {
        morningStepsCollection.getSteps().add(new LunchTimeStep().getStep());
        break;
      }
    }
  }

  @Override
  public void visit(AfternoonStepsCollection afternoonStepsCollection) {
    timeConsumed = 0;
    setPeriodTimeDuration(PeriodTimeDuration.AFTERNOON);

    for (Iterator<Step> iterator = afternoonStepsCollection.getStepsDisarray().iterator();
        iterator.hasNext(); ) {
      Step step = iterator.next();
      if (stillHaveTimeAvailable(step.getDuration())) {
        updateStep(step, AFTERNOON_PERIOD.beginsAt());
        iterator.remove();
        updateTimeConsumed(step.getDuration());
        afternoonStepsCollection.add(step);
      } else if (timeConsumed > TimeUnit.HOURS.toMinutes(3)
          && timeConsumed < TimeUnit.HOURS.toMinutes(4)) {
        Step lastStepBeforeGym =
            Collections.max(
                afternoonStepsCollection.getSteps(), Comparator.comparing(Step::getBeginsAt));
        afternoonStepsCollection.add(new LaborGymnasticsTimeStep(lastStepBeforeGym).getStep());
        break;
      }
    }
  }
}
