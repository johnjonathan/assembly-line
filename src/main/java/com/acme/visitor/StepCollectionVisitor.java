/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.visitor;

import com.acme.domain.steps.AfternoonStepsCollection;
import com.acme.domain.steps.MorningStepsCollection;

public interface StepCollectionVisitor {

  void visit(MorningStepsCollection morningStepsCollection);

  void visit(AfternoonStepsCollection afternoonStepsCollection);
}
