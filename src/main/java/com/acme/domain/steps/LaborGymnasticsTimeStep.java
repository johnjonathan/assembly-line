/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

public class LaborGymnasticsTimeStep {

  private static final String TITLE = "Labor Gymnastics";
  private Step lastStepBeforeGym;

  public LaborGymnasticsTimeStep(Step lastStepBeforeGym) {
    this.lastStepBeforeGym = lastStepBeforeGym;
  }

  public Step getStep() {
    return new Step.Builder()
        .title(TITLE)
        .duration(PeriodTimeDuration.LABOR_GYMNASTICS.time())
        .beginsAt(lastStepBeforeGym.getBeginsAt().plusMinutes(lastStepBeforeGym.getDuration()))
        .type(StepType.LABOR_GYMNASTICS)
        .build();
  }
}
