/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import com.acme.visitor.StepCollectionVisitable;
import com.acme.visitor.StepCollectionVisitor;
import java.util.List;

public class AfternoonStepsCollection extends AbstractStepsCollection
    implements StepCollectionVisitable {

  public AfternoonStepsCollection(List<Step> stepsDisarray) {
    super.initialize(stepsDisarray);
  }

  @Override
  public void accept(StepCollectionVisitor visitor) {
    visitor.visit(this);
  }
}
