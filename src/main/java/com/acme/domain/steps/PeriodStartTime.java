/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import java.time.LocalDateTime;

public enum PeriodStartTime {
  MORNING_PERIOD(LocalDateTime.now().withHour(9).withMinute(0)),
  AFTERNOON_PERIOD(LocalDateTime.now().withHour(13).withMinute(0)),
  LUNCH_PERIOD(LocalDateTime.now().withHour(12).withMinute(0));

  private LocalDateTime startTime;

  PeriodStartTime(LocalDateTime time) {
    this.startTime = time;
  }

  public LocalDateTime beginsAt() {
    return this.startTime;
  }
}
