/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

public class LunchTimeStep {

  private static final String TITLE = "Lunch time";

  public Step getStep() {
    return new Step.Builder()
        .title(TITLE)
        .duration(PeriodTimeDuration.LUNCH.time())
        .beginsAt(PeriodStartTime.LUNCH_PERIOD.beginsAt())
        .type(StepType.LUNCH_TIME)
        .build();
  }
}
