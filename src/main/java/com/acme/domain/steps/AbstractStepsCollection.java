/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractStepsCollection {

  private List<Step> steps;
  private List<Step> stepsDisarray;

  public void initialize(List<Step> stepsDisarray) {
    this.steps = new ArrayList<>();
    this.stepsDisarray = stepsDisarray;
  }

  public void add(Step step) {
    this.steps.add(step);
  }

  public List<Step> getSteps() {
    return this.steps;
  }

  public List<Step> getStepsDisarray() {
    return this.stepsDisarray;
  }
}
