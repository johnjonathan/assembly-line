/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

public enum PeriodTimeDuration {
  MORNING(180),
  AFTERNOON(240),
  LUNCH(60),
  LABOR_GYMNASTICS(0),
  MAINTENANCE(5);

  private Integer time;

  PeriodTimeDuration(Integer time) {
    this.time = time;
  }

  public Integer time() {
    return this.time;
  }
}
