/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.domain.steps;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Step {

  private Integer duration;
  private LocalDateTime beginsAt;
  private String title;
  private StepType type;

  private Step(Builder builder) {
    this.duration = builder.duration;
    this.beginsAt = builder.beginsAt;
    this.title = builder.title;
    this.type = builder.type;
  }

  public Step beginsAt(LocalDateTime hour) {
    this.beginsAt = hour;
    return this;
  }

  public Step type(StepType type) {
    this.type = type;
    return this;
  }

  public Integer getDuration() {
    return duration;
  }

  public LocalDateTime getBeginsAt() {
    return beginsAt;
  }

  public StepType getType() {
    return type;
  }

  public String getTitle() {
    return title;
  }

  @Override
  public String toString() {
    return String.format("%s %s", DateTimeFormatter.ofPattern("HH:mm").format(beginsAt), title);
  }

  public static class Builder {
    private Integer duration;
    private LocalDateTime beginsAt;
    private String title;
    private StepType type;

    public Builder duration(Integer duration) {
      this.duration = duration;
      return this;
    }

    public Builder beginsAt(LocalDateTime beginsAt) {
      this.beginsAt = beginsAt;
      return this;
    }

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder type(StepType type) {
      this.type = type;
      return this;
    }

    public Step build() {
      return new Step(this);
    }
  }
}
