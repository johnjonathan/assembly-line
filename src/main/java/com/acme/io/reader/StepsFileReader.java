/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.io.reader;

import com.acme.domain.steps.PeriodTimeDuration;
import com.acme.domain.steps.Step;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StepsFileReader {

  private static final Logger logger = Logger.getAnonymousLogger();

  private File inputTextFile;
  private List<Step> steps = new ArrayList<>();

  public StepsFileReader inputTextFile(File inputTextFile) {
    this.inputTextFile = inputTextFile;
    return this;
  }

  public StepsFileReader readSteps() {
    try (Scanner scanner = new Scanner(inputTextFile)) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        if (line.trim().isEmpty()) {
          continue;
        }
        steps.add(getStepsInfo(line));
      }
    } catch (FileNotFoundException e) {
      logger.log(Level.SEVERE, e.getMessage(), e);
    }
    return this;
  }

  public List<Step> getSteps() {
    return this.steps;
  }

  public Step getStepsInfo(String line) {
    Integer timeInMinutes = extractTimeFromTitle(line);
    return new Step.Builder().title(line).duration(timeInMinutes).build();
  }

  public Integer extractTimeFromTitle(String line) {
    String timeInMinutes = line.replaceAll("\\D", "");
    return (timeInMinutes.trim().isEmpty())
        ? PeriodTimeDuration.MAINTENANCE.time()
        : Integer.parseInt(timeInMinutes);
  }

  public File getInputTextFile() {
    return this.inputTextFile;
  }
}
