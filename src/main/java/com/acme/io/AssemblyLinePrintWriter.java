/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */
package com.acme.io;

import com.acme.domain.steps.PeriodStartTime;
import com.acme.domain.steps.Step;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class AssemblyLinePrintWriter {

  private AssemblyLinePrintWriter() {}

  /*
   * For each {@link Step} found in the list of steps
   * prints the {@link Step#toString()} in the following sample format
   *    Assembly Line 1:
   *    09:00 Cutting of steel sheets 60min
   * @param steps
   */
  public static void println(List<Step> steps) {

    BufferedWriter out =
        new BufferedWriter(
            new OutputStreamWriter(new FileOutputStream(java.io.FileDescriptor.out)));

    try (PrintWriter writer = new PrintWriter(out, true); ) {
      AtomicInteger index = new AtomicInteger();
      steps.stream()
          .forEach(
              step -> {
                if (Duration.between(step.getBeginsAt(), PeriodStartTime.MORNING_PERIOD.beginsAt())
                    .isZero()) {
                  writer.printf("%nAssembly Line: %d%n", index.incrementAndGet());
                  writer.println(String.join("", Collections.nCopies(60, "-")));
                }
                writer.println(step);
              });
    }
  }
}
